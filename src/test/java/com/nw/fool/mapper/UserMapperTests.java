package com.nw.fool.mapper;

import com.nw.fool.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

// @RunWith(SpringRunner.class)注解是一个测试启动器，可以加载Springboot测试注解
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserMapperTests {
    @Autowired(required = false)
    private UserMapper userMapper;
    /**
     * 单元测试方法：
     * 1‘必须被@Test注解修饰
     * 2、返回值类型必须是void
     * 3、方法的参数列表不指定任何类型
     * 4、方法的访问修饰符必须是public
     * */

    @Test
    public void insert(){
        User user = new User();
        user.setUsername("user05");
        user.setPassword("123456");

        Integer rows = userMapper.insert(user);
        System.out.println("rows=" + rows);
    }
    @Test
    public void findByUsername(){
        String username = "user01";
        User result = userMapper.findByUsername(username);

    }

//    @Test
//    public void updatePasswordByUid(){
//        userMapper.updatePasswordByUid(1,"321","管理员",new Date());
//    }
    @Test
    public void updatePasswordByUid() {
        Integer uid = 1;
        String password = "654321";
        String modifiedUser = "普通管理员";
        Date modifiedTime = new Date();
        Integer rows = userMapper.updatePasswordByUid(uid, password, modifiedUser, modifiedTime);
        System.out.println("rows=" + rows);
    }

    @Test
    public void findByUid(){
        Integer uid = 1;
        User result = userMapper.findByUid(uid);
        System.out.println(result);

    }

    @Test
    public void deleteByUid(){
        Integer uid = 1;
        Integer rows = userMapper.deleteByUid(uid);
        System.out.println(rows);
    }

    @Test
    public void findUserAll(){
        User user = new User();
        List<User> all= userMapper.findUserAll(user);
        System.out.println(all);
    }

    @Test
    public void  updateUserInfoByUid(){
        User user = new User();
        user.setUid(11);
        user.setUsername("李梅梅");
        user.setTelephone("11111111111");
        user.setDepart("信息中心");
        user.setStatus(0);
        user.setIsDelete(0);
        user.setModifiedUser("系统管理员");
        user.setModifiedTime(new Date());
        Integer rows = userMapper.updateUserInfoByUid(user);
        System.out.println(rows);



    }
//    @Test
//    public void selectUserTotal(){
//       Integer rows= userMapper.selectUserTotal(username);
//        System.out.println(rows);
//    }

//    @Test
//    public void loginsys(){
//        String username = "ting03";
//        String password = "123";
//    }

    @Test
    public void findByTerm(){
        User user = new User();
        user.setUsername("李");
        user.setTelephone("1");
        user.setDepart("计财");
        List<User> all=userMapper.findByTerm(user);
        System.out.println(all);

    }

    @Test
    public void updateAdminInfoByUid(){
        User user = new User();
        user.setUid(12);
        user.setTelephone("1111111111111");
        user.setDepart("计财部");
        Integer rows = userMapper.updateAdminInfoByUid(user);
        System.out.println(rows);

    }

}
