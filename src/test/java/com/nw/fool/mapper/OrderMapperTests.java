package com.nw.fool.mapper;

import com.nw.fool.entity.Order;

import com.nw.fool.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

// @RunWith(SpringRunner.class)注解是一个测试启动器，可以加载Springboot测试注解
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderMapperTests {
    @Autowired(required = false)
    private OrderMapper orderMapper;
    /**
     * 单元测试方法：
     * 1‘必须被@Test注解修饰
     * 2、返回值类型必须是void
     * 3、方法的参数列表不指定任何类型
     * 4、方法的访问修饰符必须是public
     * */

//    @Test
//    public void insert(){
//        Order order = new Order();
//        order.setCname("红烧鱼");
//        order.setCprice("2.50");
//        order.setTdtype("肉类");
//        Integer rows=orderMapper.insert(order);
//        System.out.println("rows=" + rows);
//
//    }
    @Test
    public void findByCname(){
        String cname = "红烧鱼";
        Order order =orderMapper.findByCname(cname);
        System.out.println(order);


    }

    @Test
    public void update(){
        Order order = new Order();

        Integer rows = orderMapper.update(order);
        System.out.println("rows=" + rows);


    }

    @Test
    public void findByCid(){
        Integer cid = 2;
        Order result = orderMapper.findByCid(cid);
        System.out.println(result);

    }

    @Test
    public void deleteByCid(){
        Integer cid = 1;
        Integer rows = orderMapper.deleteByCid(cid);
        System.out.println(rows);

    }


}
