package com.nw.fool.mapper;


import com.nw.fool.entity.OrderInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

// @RunWith(SpringRunner.class)注解是一个测试启动器，可以加载Springboot测试注解
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderInfoMapperTests {
    @Autowired(required = false)
    private OrderInfoMapper orderInfoMapper;
    /**
     * 单元测试方法：
     * 1‘必须被@Test注解修饰
     * 2、返回值类型必须是void
     * 3、方法的参数列表不指定任何类型
     * 4、方法的访问修饰符必须是public
     * */

    @Test
    public void insert(){
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setUid(1);
        orderInfo.setDclist("米饭，大米饭");
//        orderInfo.setDcprice("2.50");
        orderInfo.setDccount(2);
        orderInfo.setDctype("午餐");
        Integer rows=orderInfoMapper.insert(orderInfo);
        System.out.println("rows=" + rows);

    }



}
