package com.nw.fool.mapper;

import com.nw.fool.entity.Menu;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

// @RunWith(SpringRunner.class)注解是一个测试启动器，可以加载Springboot测试注解
@RunWith(SpringRunner.class)
@SpringBootTest
public class MenuMapperTests {
    @Autowired(required = false)
    private MenuMapper menuMapper;
    /**
     * 单元测试方法：
     * 1‘必须被@Test注解修饰
     * 2、返回值类型必须是void
     * 3、方法的参数列表不指定任何类型
     * 4、方法的访问修饰符必须是public
     * */

    @Test
    public void insert(){
        Menu menu = new Menu();
        menu.setCname("红烧肉");
        menu.setCprice("2.50");
        Integer rows = menuMapper.insert(menu);
        System.out.println("rows = "+rows);
    }



}
