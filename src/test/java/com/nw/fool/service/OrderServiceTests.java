package com.nw.fool.service;


import com.nw.fool.entity.Order;
import com.nw.fool.entity.User;
import com.nw.fool.service.ex.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

// @RunWith(SpringRunner.class)注解是一个测试启动器，可以加载Springboot测试注解
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceTests {
    @Autowired(required = false)
    private IOrderService orderService;
    /**
     * 单元测试方法：
     * 1‘必须被@Test注解修饰
     * 2、返回值类型必须是void
     * 3、方法的参数列表不指定任何类型
     * 4、方法的访问修饰符必须是public
     * */
//    @Test
//    public void insert(){
//        try {
//            Order order = new Order();
//            order.setCname("红烧肉");
//            order.setCprice("2.00");
//            order.setTdtype("肉类");
//            orderService.insert(order);
//            System.out.println("Ok");
//        } catch (ServiceException e) {
//            //获取类的对象，再获取类的名称
//            System.out.println(e.getClass().getSimpleName());
//            //获取异常的具体描述信息
//            System.out.println(e.getMessage());
//        }
//
//    }

    @Test
    public void findByCname(){
       Order order = orderService.findByCname("红烧肉");
        System.out.println(order);

    }

    @Test
    public void deleteByCid(){
        orderService.deleteByCid(2);
    }


}
