package com.nw.fool.service;

import com.nw.fool.entity.User;
import com.nw.fool.service.ex.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.ws.Service;
import java.util.Date;
import java.util.List;

// @RunWith(SpringRunner.class)注解是一个测试启动器，可以加载Springboot测试注解
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTests {
    @Autowired(required = false)
    private IUserService userService;
    /**
     * 单元测试方法：
     * 1‘必须被@Test注解修饰
     * 2、返回值类型必须是void
     * 3、方法的参数列表不指定任何类型
     * 4、方法的访问修饰符必须是public
     * */
    @Test
    public void reg(){
        try {
            User user = new User();
            user.setUsername("user");
            user.setPassword("123");
            user.setTelephone("1111111111111");
            user.setDepart("信息中西");
            userService.reg(user);
            System.out.println("Ok");
        } catch (ServiceException e) {
            //获取类的对象，再获取类的名称
            System.out.println(e.getClass().getSimpleName());
            //获取异常的具体描述信息
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void login(){
        User user = userService.login("ting03","123");
        System.out.println(user);
    }

    @Test
    public void changePassword(){
        userService.changePassword(10,"ting07","123","321");
    }

    @Test
    public void delete(){
        userService.delete(9);
    }

    @Test
    public void findUserAll(){
        User user = new User();
        List<User> all= userService.findUserAll(user);
        System.out.println(all);

    }
    @Test
    public void updateUserInfoByUid(){
        User user = new User();
        user.setUid(11);
        user.setUsername("李青青");
        user.setTelephone("1299999999");
        user.setDepart("计财部");
        user.setStatus(1);
        user.setIsDelete(1);
        userService.updateUserInfoByUid(user);
        System.out.println("ok");

    }

    @Test
    public void findByTerm(){
        User user = new User();
        user.setUsername("test");
        user.setTelephone("1000");
        user.setDepart("计财");
        List<User> all  = userService.findByTerm(user);
        System.out.println(all);


    }

    @Test
    public void updateAdminInfoByUid(){
        User user = new User();
        user.setUid(12);
        user.setUsername("李青青");
        user.setTelephone("1299999999");
        user.setDepart("计财部");
        userService.updateAdminInfoByUid(user);
        System.out.println("ok");
    }


}
