package com.nw.fool.service;

import com.nw.fool.entity.Menu;
import com.nw.fool.entity.OrderInfo;
import com.nw.fool.entity.User;
import com.nw.fool.service.ex.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

// @RunWith(SpringRunner.class)注解是一个测试启动器，可以加载Springboot测试注解
@RunWith(SpringRunner.class)
@SpringBootTest
public class MenuServiceTests {
    @Autowired(required = false)
    private IMenuService menuService;
    /**
     * 单元测试方法：
     * 1‘必须被@Test注解修饰
     * 2、返回值类型必须是void
     * 3、方法的参数列表不指定任何类型
     * 4、方法的访问修饰符必须是public
     * */

    @Test
    public void insert(){
        try {
            Menu menu = new Menu();
            menu.setCname("空心菜");
            menu.setCprice("1.50");
            menuService.insert(menu);
            System.out.println("Ok");
        } catch (ServiceException e) {
            //获取类的对象，再获取类的名称
            System.out.println(e.getClass().getSimpleName());
            //获取异常的具体描述信息
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void findMenuAll(){
        Menu menu = new Menu();
        List<Menu> all = menuService.findMenuAll(menu);
        System.out.println(all);
    }

    @Test
    public void updateMenuInfoByCid(){
        Menu menu = new Menu();
        menu.setCid(1);
        menu.setCname("菜心");
        menu.setCprice("1.50");
        menuService.updateMenuInfoByCid(menu);
        System.out.println("ok");

    }

    @Test
    public void deleteMenuByCid(){
        menuService.deleteMenuByCid(2);
    }

    @Test
    public void findMenuByCname(){
        Menu menu = new Menu();
        menu.setCname("菜心");
        List<Menu> all = menuService.findMenuByCname(menu);
        System.out.println(all);

    }




}
