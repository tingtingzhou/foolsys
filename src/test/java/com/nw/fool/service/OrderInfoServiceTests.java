package com.nw.fool.service;


import com.nw.fool.entity.Order;
import com.nw.fool.entity.OrderInfo;
import com.nw.fool.service.ex.ServiceException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

// @RunWith(SpringRunner.class)注解是一个测试启动器，可以加载Springboot测试注解
@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderInfoServiceTests {
    @Autowired(required = false)
    private IOrderInfoService iOrderInfoService;
    /**
     * 单元测试方法：
     * 1‘必须被@Test注解修饰
     * 2、返回值类型必须是void
     * 3、方法的参数列表不指定任何类型
     * 4、方法的访问修饰符必须是public
     * */
    @Test
    public void insert(){
        try {
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setUid(2);
            orderInfo.setDclist("米饭，大白菜");
//            orderInfo.setDcprice("2.00");
            orderInfo.setDccount(2);
            orderInfo.setDctype("晚餐");
            iOrderInfoService.insert(orderInfo);
            System.out.println("Ok");
        } catch (ServiceException e) {
            //获取类的对象，再获取类的名称
            System.out.println(e.getClass().getSimpleName());
            //获取异常的具体描述信息
            System.out.println(e.getMessage());
        }

    }




}
