package com.nw.fool.mapper;

import com.nw.fool.entity.User;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

/** 处理用户数据操作的持久层接口 */
public interface UserMapper {

    /**
     * 插入用户数据
     * @param user 用户数据
     * @return 受影响行数
     */
    Integer insert(User user);

    /**
     * 根据用户名查找用户数据
     * @param username 用户名
     * @return 匹配的用户数据，如果没有匹配的数据，则返回null
     */
    User findByUsername(String username);

    /**
     * 根据用户的uid修改用户密码
     * @param uid 用户的uid
     * @param password  用户输入的新密码
     * @param modifiedUser 表示修改的执行者
     * @param modifiedTime 表示修改数据的时间
     * @return 返回值为受影响的行数
     */

    Integer updatePasswordByUid(Integer uid,
                                String password,
                                String modifiedUser,
                                Date modifiedTime);


    /**
     * 根据用户的id查询用户的数据
     * @param uid  用户的id
     * @return  如果找到则返回对象，反之返回null
     */
    User findByUid(Integer uid);




    /**
     * 根据用户id删除用户数据
     * @param uid 用户的id
     * @return  返回受影响的行数
     */
    Integer deleteByUid(Integer uid);


    /**
     *查找用户表全部数据
     * @param user 用户数据
     * @return 如果找到则返回对象，反之返回null
     */
    List<User> findUserAll(User user);

    /**
     *修改用户信息
     * @param user
     * @return
     */
   Integer updateUserInfoByUid(User user);
//    void updateUserInfoByUid(User user);

    List<User> selectPage(Integer pageNum, Integer pageSize,String username);

    Integer selectUserTotal(String username);

    Integer loginsys(String username,String password);

    /**
     *根据姓名，电话号码，部门多条件查询用户数据
     * @param user 用户数据
     * @return 返回受影响的行数
     */
    List<User> findByTerm(User user);


    /**
     * 根据uid修改管理者的信息
     * @param user 管理者用户的消息
     * @return 返回受影响的行数
     */
    Integer updateAdminInfoByUid(User user);


    /**
     * 根据用户id修改密码
     * @param user
     * @return
     */
    Integer updatePasswordInfoByUid(User user);


    /**
     * 批量新增用户
     * @param user
     * @return
     */
    Integer insertUserList(List<User> user);


    /**
     * 通过电话号码实现登录
     * @param telephone 用户的电话号码
     * @param password 用户输入的密码
     * @return
     */
    User loginByTelephone(String telephone,String password);


    /**
     * 通过用户的手机号查找用户信息
     * @param telephone
     * @return
     */
    User findUserByTelephone(String telephone);




}
