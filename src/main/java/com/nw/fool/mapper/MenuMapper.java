package com.nw.fool.mapper;

import com.nw.fool.entity.Menu;


import java.util.Date;
import java.util.List;

/** 处理菜谱表数据操作的持久层接口 */
public interface MenuMapper {

    /**
     * 插入菜谱数据
     * @param menu 菜谱实体数据
     * @return 受影响行数
     */
    Integer insert(Menu menu);

    /**
     * 查找菜谱表的全部数据
     * @param menu
     * @return
     */
    List<Menu> findMenuAll(Menu menu);

    /**
     * 修改菜谱信息
     * @param menu 菜谱实体
     * @return
     */
    Integer updateMenuInfoByCid(Menu menu);


    /**
     * 根据菜谱cid删除菜谱信息
     * @param cid 菜谱id
     * @return 返回受影响的行数
     */
    Integer deleteMenuByCid(Integer cid);


    /**
     * 根据菜谱id删除菜谱数据
     * @return
     */
    Menu findMenuByCid();


    /**
     * 根据菜名条件查询菜谱数据
     * @param menu 菜谱实体
     * @return 返回受影响行数
     */
    List<Menu> findMenuByCname(Menu menu);

    /**
     * 根据多个id进行批量删除
     * @param cids
     * @return
     */
    Integer deleteMenuByCids(List<Integer> cids);







}
