package com.nw.fool.mapper;

import com.nw.fool.entity.Order;

import java.util.List;

public interface OrderMapper {
    /**
     * 插入菜单数据
     * @param order 菜单数据
     * @return 受影响行数
     */
    Integer insert(Order order);


    /**
     * 根据菜名查找菜单数据
     * @param cname  菜名
     * @return 匹配的菜单数据，如果没有匹配的数据，则返回null
     */
    Order findByCname(String cname);

    /**
     * 修改今日菜谱
     * @param order 今日菜谱的数据
     * @return 返回受影响的行数
     */
    Integer update(Order order);

    /**
     *根据菜谱id查找菜谱数据
     * @param cid
     * @return
     */
    Order findByCid(Integer cid);

    /**
     * 根据菜谱id删除菜谱数据
     * @param cid 今日菜谱的id
     * @return 返回受影响的行数
     */
    Integer deleteByCid(Integer cid);

    /**
     * 根据订餐类型查询今日菜谱
     * @param tdtype 今日菜谱的订餐类型
     * @return 返回匹配的菜谱数据，如果没有匹配的数据，则返回null
     */
    List<Order> findByTdtype(String tdtype);

    /**
     * 根据多个cid进行批量修改
     *
     * @return
     */
    Integer updateSureByCids(List<Order> order);


    /**
     * 批量插入发布菜谱数据
     * @param order 菜谱实体
     * @return 返回受影响行数
     */
    Integer insertOrderInfoList(List<Order> order);


    /**
     * 批量插入预设明日菜谱数据
     * @param order
     * @return 返回受影响行数
     */
    Integer preInsertOrderInfoList(List<Order> order);

    /**
     * 根据订餐类型和明天时间查看预设菜谱
     * @param tdtype 今日菜谱的订餐类型
     * @return
     */
    List<Order> findOrderByTdtypeAndTime(String tdtype);





}
