package com.nw.fool.service;

import com.nw.fool.entity.Menu;
import com.nw.fool.entity.User;

import java.util.List;

/**菜谱模块业务层接口*/

public interface IMenuService {

    /**
     * 插入菜谱数据的方法
     * @param menu 菜谱的数据对象
     */
    void insert(Menu menu);

    /**
     * 查找菜谱所有数据
     * @param menu 菜谱实体
     * @return
     */
    List<Menu> findMenuAll(Menu menu);


    /**
     * 修改菜谱信息
     * @param menu 菜谱实体
     */
    void updateMenuInfoByCid(Menu menu);


    /**
     * 删除菜谱信息
     * @param cid 菜谱信息id
     */
    void deleteMenuByCid(Integer cid);

    /**
     * 根据菜谱名条件查询
     * @param menu 菜谱实体
     * @return 返回受影响行数
     */
    List<Menu> findMenuByCname(Menu menu);

    /**
     * 批量删除菜谱信息
     * @param cids 菜谱信息c ids
     */
    void deleteMenuByCids(List<Integer> cids);
}
