package com.nw.fool.service;

import com.nw.fool.dto.UpdatePassworDto;
import com.nw.fool.entity.User;

import java.util.List;

/**用户模块业务层接口*/

public interface IUserService {
    /**
     * 用户注册的方法
     * @param user 用户的数据对象
     */
    void reg(User user);

    /**
     * 用户登录
     * @param username 用户名
     * @param password 密码
     * @return 登录成功的数据
     */
    User login(String username,String password);

    /**
     *修改用户密码
     * @param uid
     * @param username
     * @param oldPassword
     * @param newPassword
     */
    void changePassword(Integer uid,
                        String username,
                        String oldPassword,
                        String newPassword
                        );

    /**
     *删除用户
     * @param uid 用户id
     */
    void delete(Integer uid);


    /**
     * 查找用户所有数据
     * @param user
     * @return
     */
    List<User> findUserAll(User user);


    /**
     * 修改用户
     * @param user
     */
    void updateUserInfoByUid(User user);


    List<User> selectPage(Integer pageNum, Integer pageSize,String username);

    Integer selectUserTotal(String username);
    Integer loginsys(String username,String password);

    /**
     * 根据姓名，电话号码，部门多条件查询
     * @param user
     * @return
     */
    List<User> findByTerm(User user);

    /**
     * 根据uid修改管理者的信息
     * @param user
     */
    void updateAdminInfoByUid(User user);

    /**
     * 根据用户id修改密码
     * @param updatePassworDto 更新passwor dto
     */
    void updatePasswordInfoByUid(UpdatePassworDto updatePassworDto);



    /**
     * 批量新增用户
     * @param user
     * @return
     */
    Integer insertUserList(List<User> user);

    /**
     * 通过电话号码实现登录
     * @param telephone 用户的电话号码
     * @param password 用户输入的密码
     * @return
     */
    User loginByTelephone(String telephone,String password);

    /**
     * 通过用户的手机号查找用户信息
     * @param telephone
     * @return
     */
    User findUserByTelephone(String telephone);


    /**
     * 根据用户的id查询用户的数据
     * @param uid  用户的id
     * @return  如果找到则返回对象，反之返回null
     */
    User findByUid(Integer uid);



}
