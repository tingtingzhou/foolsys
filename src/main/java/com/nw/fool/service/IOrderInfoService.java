package com.nw.fool.service;

import com.nw.fool.entity.OrderInfo;

import java.util.List;

/**订餐信息业务层接口*/
public interface IOrderInfoService {

    /**
     * 插入订餐信息
     * @param orderInfo 订餐信息对象
     */
    void insert(OrderInfo orderInfo);


    /**
     * 根据订餐类型查找数据
     * @param dctype
     * @return 匹配的订餐信息数据，如果没有匹配的数据，则返回null
     */
    List<OrderInfo> findOrderInfoByDctype(String dctype);

    /**
     * 根据订餐id和订餐类型删除数据
     * @param dcid
     * @param dctype
     */
    void deleteByDcidAndDctype(Integer dcid,String dctype);

    /**
     * 查找订餐信息全部数据
     * @param orderInfo 订餐信息实体
     * @return 返回实体类型订餐信息数据列表
     */
    List<OrderInfo> findOrderInfoAll(OrderInfo orderInfo);

    /**
     * 根据订餐时间段查询数据
     * @param dctimeStart 开始时间
     * @param dctimeEnd 结束时间
     * @return 返回实体类型订餐信息数据列表
     */
    List<OrderInfo> findOrderInfoByDctime(String dctimeStart,String dctimeEnd);

    /**
     * 根据用户姓名查询数据
     * @param username
     * @return 返回实体类型订餐信息数据列表
     */
    List<OrderInfo> findOrderInfoByUsername(String username);

    /**
     * 根据多条件查询订餐信息数据列表
     * @param dctimeYear 订餐时间年份
     * @param dctimeMonth 订餐时间月份
     * @param dcdepart 订餐部门
     * @param username 订餐用户
     * @return
     */
    List<OrderInfo> findOrderInfoByConditions(String dctimeYear,String dctimeMonth,String dcdepart,String username);


    /**
     * 根据月时间段查询订餐信息列表数据
     * @param dctimeYear 订餐时间年份
     * @param dctimeMonth 订餐时间月份
     * @return
     */
    List<OrderInfo> findOrderInfoBydctimeYearAnddctimeMonth(String dctimeYear,String dctimeMonth);

    /**
     * 根据月时间段和uid查询查询订餐信息列表数据
     * @param dctimeYear 订餐时间年份
     * @param dctimeMonth 订餐时间月份
     * @param uid 订餐人的ID
     * @return
     */
    List<OrderInfo> findOrderInfoBydctimeYearAnddctimeMonthAndUid(String dctimeYear, String dctimeMonth, Integer uid);

    /**
     * 根据订餐时间查询订餐信息列表
     * @param uid 用户id
     * @param
     * @return
     */
    List<OrderInfo> findOrderInfoByUidAndDctime(Integer uid,String dctype);


    /**
     * 计算订餐信息有多少条
     * @param dctype 订餐类型
     * @return
     */
    List<OrderInfo> orderInfoCount(String dctype);

}
