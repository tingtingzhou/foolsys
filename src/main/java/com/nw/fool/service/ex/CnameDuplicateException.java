package com.nw.fool.service.ex;
/**菜名被占用异常*/
public class CnameDuplicateException extends ServiceException{
    public CnameDuplicateException() {
        super();
    }

    public CnameDuplicateException(String message) {
        super(message);
    }

    public CnameDuplicateException(String message, Throwable cause) {
        super(message, cause);
    }

    public CnameDuplicateException(Throwable cause) {
        super(cause);
    }

    protected CnameDuplicateException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
