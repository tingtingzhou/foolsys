package com.nw.fool.service.ex;

/**
 * 订餐信息已存在异常
 * */
public class OrderInfoHasExistedException extends ServiceException{
    public OrderInfoHasExistedException() {
    }

    public OrderInfoHasExistedException(String message) {
        super(message);
    }

    public OrderInfoHasExistedException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrderInfoHasExistedException(Throwable cause) {
        super(cause);
    }

    public OrderInfoHasExistedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
