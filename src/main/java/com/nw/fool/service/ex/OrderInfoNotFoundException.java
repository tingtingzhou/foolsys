package com.nw.fool.service.ex;

public class OrderInfoNotFoundException extends ServiceException{
    public OrderInfoNotFoundException() {
    }

    public OrderInfoNotFoundException(String message) {
        super(message);
    }

    public OrderInfoNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public OrderInfoNotFoundException(Throwable cause) {
        super(cause);
    }

    public OrderInfoNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
