package com.nw.fool.service;

import com.nw.fool.entity.Order;

import java.util.List;


public interface IOrderService {

    /**
     * 插入菜单数据的方法
     * @param order  菜单的数据对象
     */
    void insert(Order order);

    /**
     * 根据菜名查找菜单数据
     * @param cname
     * @return
     */
    Order findByCname(String cname);

    Integer update(Order order);

    void deleteByCid(Integer cid);

    List<Order> findByTdtype(String tdtype);

    /**
     *根据菜谱id查找菜谱数据
     * @param cid
     * @return
     */
    Order findByCid(Integer cid);

    /**
     * 根据多个cid进行批量修改
     * @param
//     * @param time
     * @return
     */
    void updateSureByCids(List<Order> order);

    /**
     * 批量增加菜谱
     * @param order
     */
    void insertOrderInfoList(List<Order> order);

    /**
     * 批量预设增加菜谱
     * @param order
     */
    void preInsertOrderInfoList(List<Order> order);

    /**
     * 根据订餐类型和明天时间查看预设菜谱
     * @param tdtype 今日菜谱的订餐类型
     * @return
     */
    List<Order> findOrderByTdtypeAndTime(String tdtype);

}
