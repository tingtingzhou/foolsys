package com.nw.fool.service.impl;

import com.nw.fool.entity.Menu;
import com.nw.fool.entity.User;
import com.nw.fool.mapper.MenuMapper;

import com.nw.fool.service.IMenuService;
import com.nw.fool.service.IUserService;
import com.nw.fool.service.ex.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**菜谱模块业务层的实现类*/
@Service  //@Service 注解：将当前类的对象交给spring来管理，自动创建对象以及对象的维护
public class MenuServiceImpl implements IMenuService {
    @Autowired(required = false)
    private MenuMapper menuMapper;


    @Override
    public void insert(Menu menu) {
        menu.setCreatedUser(menu.getCreatedUser());
        menu.setModifiedUser(menu.getModifiedUser());
        Date date = new Date();
        menu.setCreatedTime(date);
        menu.setModifiedTime(date);
        Integer rows = menuMapper.insert(menu);
        if (rows != 1){
            throw new InsertException("插入菜谱信息数据产生未知异常");
        }

    }

    @Override
    public List<Menu> findMenuAll(Menu menu) {
        List<Menu> all= menuMapper.findMenuAll(menu);
        return all;
    }

    @Override
    public void updateMenuInfoByCid(Menu menu) {
        menu.getCid();
//        menu.setModifiedUser();
        menu.setModifiedTime(new Date());
        Integer rows = menuMapper.updateMenuInfoByCid(menu);
        if(rows != 1){
            throw new UpdateException("更新用户数据时出现未知错误");

        }
    }

    @Override
    public void deleteMenuByCid(Integer cid) {

       Integer rows = menuMapper.deleteMenuByCid(cid);
        if(rows !=1){
            throw  new DeleteException("删除数据产生未知的异常");
        }
    }

    @Override
    public List<Menu> findMenuByCname(Menu menu) {
       List<Menu> all = menuMapper.findMenuByCname(menu);
        return all;
    }

    @Override
    public void deleteMenuByCids(List<Integer> cids) {

        Integer rows = menuMapper.deleteMenuByCids(cids);
    }
}
