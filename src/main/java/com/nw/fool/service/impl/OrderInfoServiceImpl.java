package com.nw.fool.service.impl;

import com.nw.fool.entity.OrderInfo;
import com.nw.fool.mapper.OrderInfoMapper;
import com.nw.fool.service.IOrderInfoService;
import com.nw.fool.service.ex.DeleteException;
import com.nw.fool.service.ex.InsertException;
import com.nw.fool.service.ex.OrderInfoHasExistedException;
import com.nw.fool.service.ex.OrderInfoNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**订餐信息模块业务层的实现类*/
@Service  //@Service 注解：将当前类的对象交给spring来管理，自动创建对象以及对象的维护
public class OrderInfoServiceImpl implements IOrderInfoService {
    @Autowired(required = false)
    private OrderInfoMapper orderInfoMapper;
    @Override
    public void insert(OrderInfo orderInfo) {
        List<OrderInfo> data = orderInfoMapper.findOrderInfoByUidAndDctime(orderInfo.getUid(),orderInfo.getDctype());
        if(data != null ){
            throw new OrderInfoHasExistedException("今日订餐信息已存在");
        }


        orderInfo.setCreatedUser(orderInfo.getCreatedUser());
        orderInfo.setModifiedUser(orderInfo.getModifiedUser());
        Date date = new Date();
        orderInfo.setCreatedTime(date);
        orderInfo.setModifiedTime(date);
        Integer rows = orderInfoMapper.insert(orderInfo);
        if(rows !=1){
            throw new InsertException("插入订餐信息数据产生未知的异常");

        }


    }

    @Override
    public List<OrderInfo> findOrderInfoByDctype(String dctype) {
        List<OrderInfo> orderInfo=orderInfoMapper.findOrderInfoByDctype(dctype);
        if(orderInfo == null){
            throw new OrderInfoNotFoundException("订餐信息不存在异常");
        }
        return orderInfo;
    }

    @Override
    public void deleteByDcidAndDctype(Integer dcid, String dctype) {
        Integer rows = orderInfoMapper.deleteByDcidAndDctype(dcid,dctype);
        if(rows != 1){
            throw new DeleteException("删除今日菜谱数据产生未知的异常");
        }

    }

    @Override
    public List<OrderInfo> findOrderInfoAll(OrderInfo orderInfo) {
        List<OrderInfo> all = orderInfoMapper.findOrderInfoAll(orderInfo);
        return all;
    }

    @Override
    public List<OrderInfo> findOrderInfoByDctime(String dctimeStart, String dctimeEnd) {
        List<OrderInfo> all = orderInfoMapper.findOrderInfoByDctime(dctimeStart,dctimeEnd);
        return all;
    }

    @Override
    public List<OrderInfo> findOrderInfoByUsername(String username) {
        List<OrderInfo> all = orderInfoMapper.findOrderInfoByUsername(username);
        return all;
    }

    @Override
    public List<OrderInfo> findOrderInfoByConditions(String dctimeYear, String dctimeMonth, String dcdepart, String username) {
       List<OrderInfo> all = orderInfoMapper.findOrderInfoByConditions(dctimeYear,dctimeMonth,dcdepart,username);
        return all;
    }

    @Override
    public List<OrderInfo> findOrderInfoBydctimeYearAnddctimeMonth(String dctimeYear, String dctimeMonth) {
        List<OrderInfo> all = orderInfoMapper.findOrderInfoBydctimeYearAnddctimeMonth(dctimeYear,dctimeMonth);
        return all;
    }

    @Override
    public List<OrderInfo> findOrderInfoBydctimeYearAnddctimeMonthAndUid(String dctimeYear, String dctimeMonth, Integer uid) {
        List<OrderInfo> all = orderInfoMapper.findOrderInfoBydctimeYearAnddctimeMonthAndUid(dctimeYear,dctimeMonth,uid);
        return all;
    }

    @Override
    public List<OrderInfo> findOrderInfoByUidAndDctime(Integer uid,String dctype) {
        List<OrderInfo> all = orderInfoMapper.findOrderInfoByUidAndDctime(uid,dctype);
        return all;
    }

    @Override
    public List<OrderInfo> orderInfoCount(String dctype) {
        List<OrderInfo> all = orderInfoMapper.orderInfoCount(dctype);
        return all;
    }


}
