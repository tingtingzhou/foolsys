package com.nw.fool.service.impl;

import com.nw.fool.dto.UpdatePassworDto;
import com.nw.fool.entity.User;
import com.nw.fool.mapper.UserMapper;
import com.nw.fool.service.IUserService;
import com.nw.fool.service.ex.*;
import com.nw.fool.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**用户模块业务层的实现类*/
@Service  //@Service 注解：将当前类的对象交给spring来管理，自动创建对象以及对象的维护
public class UserServiceImpl implements IUserService {
    @Autowired(required = false)
    private UserMapper userMapper;
    @Override
    public void reg(User user) {
        //通过user参数来获取传递过来的username
        String username = user.getUsername();
        //调用findByUsername（username）判断用户是否被注册过
        User result = userMapper.findByUsername(username);
        //判断结果集是否不为null，则抛出用户名被占用异常
        if(result != null){
            //抛出异常
            throw new UsernameDuplicateException("用户名被占用");
        }


        //密码加密处理的实现：MD5算法形式
        //(串+password+串) --------md5算法进行加密,连续加载三次
        //盐值+password+盐值--------盐值就是一个随机字符串
        String oldPassword = user.getPassword();
        //获取盐值（随机生成一个盐值）
        String salt = UUID.randomUUID().toString().toUpperCase();
        //补全数据：盐值的记录
        user.setSalt(salt);
        //将密码和盐值作为一个整体进行加密处理
       String md5Password = getMD5Password(oldPassword,salt);
       //将加密之后的密码重新补全设置到user对象中
        user.setPassword(md5Password);



        //补全数据：is_delete设置为0
        user.setIsDelete(0);
        user.setStatus(0);
        //补全数据：4个日志字段信息
        user.setCreatedUser(user.getCreatedUser());
        user.setModifiedUser(user.getUsername());
        Date date = new Date();
        user.setCreatedTime(date);
        user.setModifiedTime(date);
        //执行注册业务功能的实现(row==1)
        Integer rows = userMapper.insert(user);
        if(rows != 1){
            throw new InsertException("在用户注册过程中产生了位置的异常");
        }
    }

    @Override
    public User login(String username, String password) {
        //根据用户名来查询用户数据是否存在，如果不在则抛出异常
        User result = userMapper.findByUsername(username);
        if(result==null){
            throw new UserNotFoundException("用户数据不存在");
        }
        //检测用户的密码是否匹配
        //1、先获取到数据库中加密之后的密码
         String oldPassword = result.getPassword();
        //2、和用户传递过来的密码进行比较
        //2.1先获取盐值：上一次注册时搜自动生成的盐值
        String salt = result.getSalt();
        //2.2将用户的密码按照MD5算法进行加密
        String newMd5Password = getMD5Password(password,salt);
        //3、将密码进行比较
        if(!newMd5Password.equals(oldPassword)){
            throw new PasswordNotMatchException("用户密码错误");
        }
        //判断is_delete字段值是否为1表示标记为删除
        if(result.getIsDelete()==1){
            throw  new UserNotFoundException("用户数据不存在");
        }
        //调用Mappers层的findByUsername来查询用户数据
        //User user = userMapper.findByUsername(username);
        User user = new User();
        user.setUid(result.getUid());
        user.setUsername(result.getUsername());
        user.setStatus(result.getStatus());
        user.setDepart(result.getDepart());
        user.setTelephone(result.getTelephone());

        //将当前用户返回
        return user;
    }

    //修改密码
    @Override
    public void changePassword(Integer uid,
                               String username,
                               String oldPassword,
                               String newPassword) {
    User result = userMapper.findByUid(uid);
    if(result == null || result.getIsDelete() ==1){
        throw new UserNotFoundException("用户数据不存在");
    }
    //原始密码与数据库中的密码进行比较
     String  oldMd5Password = getMD5Password(oldPassword,result.getSalt());
    if(!result.getPassword().equals(oldMd5Password)){
        throw new PasswordNotMatchException("密码错误");
    }
    //将新的密码设置到数据库中，将新的密码进行加密再去更新
    String newMd5Password  = getMD5Password(newPassword, result.getSalt());
    Integer rows = userMapper.updatePasswordByUid(uid,
                                    newMd5Password,
                                    username,
                                    new Date());

    if (rows != 1){
        throw  new UpdateException("更新数据产生未知异常");
    }

    }

    //删除用户
    @Override
    public void delete(Integer uid) {
        User result = userMapper.findByUid(uid);
        if(result == null){
            throw new UserNotFoundException("用户数据不存在");
        }
        Integer rows =userMapper.deleteByUid(uid);
        if(rows !=1){
            throw  new DeleteException("删除数据产生未知的异常");
        }


    }

    @Override
    public List<User> findUserAll(User user) {
        List<User> all = userMapper.findUserAll(user);
        return all;
    }

    @Override
    public void updateUserInfoByUid(User user) {
        //通过user参数来获取传递过来的uid
        user.getUid();
        user.setModifiedUser(user.getUsername());
        user.setModifiedTime(new Date());
        Integer rows = userMapper.updateUserInfoByUid(user);
        if(rows != 1){
            throw new UpdateException("更新用户数据时出现未知错误");


        }
    }

    @Override
    public List<User> selectPage(Integer pageNum, Integer pageSize,String username) {
        List<User> all =userMapper.selectPage(pageNum,pageSize,username);
        return all;
    }

    @Override
    public Integer selectUserTotal(String username) {
       return userMapper.selectUserTotal(username);

    }

    @Override
    public Integer loginsys(String username, String password) {
        return userMapper.loginsys(username,password);
    }

    @Override
    public List<User> findByTerm(User user) {
       List<User>  all =userMapper.findByTerm(user);
        return all;
    }

    @Override
    public void updateAdminInfoByUid(User user) {
        //通过user参数来获取传递过来的username
        user.getUid();
        Integer rows = userMapper.updateAdminInfoByUid(user);
        if(rows != 1){
            throw new UpdateException("更新用户数据时出现未知错误");

        }

    }

    @Override
    public void updatePasswordInfoByUid(UpdatePassworDto updatePassworDto) {
//        Integer uid = user.getUid();
        User result = userMapper.findByUid(updatePassworDto.getUid());
        if(result == null ){
            throw new UserNotFoundException("用户数据不存在");
        }
        //原始密码与数据库中的密码进行比较
        String  oldMd5Password = getMD5Password(updatePassworDto.getOldPassword(),result.getSalt());
        if(!result.getPassword().equals(oldMd5Password)){
            throw new PasswordNotMatchException("密码错误");
        }
        //将新的密码设置到数据库中，将新的密码进行加密再去更新
        String newMd5Password  = getMD5Password(updatePassworDto.getNewPassword(), result.getSalt());
        //更新密码到实体对象
        result.setPassword(newMd5Password);
        Integer rows = userMapper.updatePasswordInfoByUid(result);
        if (rows != 1){
            throw  new UpdateException("更新数据产生未知异常");
        }


    }

    @Override
    public Integer insertUserList(List<User> user) {
        return userMapper.insertUserList(user);
    }

    @Override
    public User loginByTelephone(String telephone, String password) {
        //根据用户名来查询用户数据是否存在，如果不在则抛出异常
        User result = userMapper.findUserByTelephone(telephone);
        if (result==null){
            throw new UserNotFoundException("用户数据不存在");
        }
        //检测用户的密码是否匹配
        //1、先获取到数据库中加密之后的密码
        String oldPassword = result.getPassword();
        //2、和用户传递过来的密码进行比较
        //2.1先获取盐值：上一次注册时搜自动生成的盐值
        String salt = result.getSalt();
        //2.2将用户的密码按照MD5算法进行加密
        String newMd5Password = getMD5Password(password,salt);
        //3、将密码进行比较
        if(!newMd5Password.equals(oldPassword)){
            throw new PasswordNotMatchException("用户密码错误");
        }
        //判断is_delete字段值是否为1表示标记为删除
        if(result.getIsDelete()==1){
            throw  new UserNotFoundException("用户数据不存在");
        }
        //调用Mappers层的findByUsername来查询用户数据
        //User user = userMapper.findByUsername(username);
        User user = new User();
        user.setUid(result.getUid());
        user.setUsername(result.getUsername());
        user.setStatus(result.getStatus());
        user.setDepart(result.getDepart());
        user.setTelephone(result.getTelephone());


        //设置Token
        String token = TokenUtil.getToken(result.getUid().toString(),result.getPassword());

        user.setToken(token);
//        user.setToken(result.getToken());
        //将当前用户返回
        return user;
    }

    @Override
    public User findUserByTelephone(String telephone) {
        User result = userMapper.findUserByTelephone(telephone);
        return result;
    }

    @Override
    public User findByUid(Integer uid) {
        User result = userMapper.findByUid(uid);
        return result;
    }



//    @Override
//    public void updatePasswordInfoByUid(User user) {
//        Integer uid = user.getUid();
//        User result = userMapper.findByUid(uid);
////        if(result == null || result.getIsDelete() ==1){
////            throw new UserNotFoundException("用户数据不存在");
////        }
//
//
//
//    }


    //定义一个MD5算法进行加密
    private String getMD5Password(String password,String salt){
        for(int i = 0; i < 3 ;i++) {
            //md5加密算法方法的调用（进行三次）
            password = DigestUtils.md5DigestAsHex((salt + password + salt).getBytes()).toUpperCase();
        }
        //返回加密后的密码
        return password;
    }
}
