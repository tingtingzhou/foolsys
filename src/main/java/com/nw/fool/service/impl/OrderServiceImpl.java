package com.nw.fool.service.impl;

import com.nw.fool.entity.Order;
import com.nw.fool.mapper.OrderMapper;
import com.nw.fool.service.IOrderService;
import com.nw.fool.service.ex.CnameDuplicateException;
import com.nw.fool.service.ex.DeleteException;
import com.nw.fool.service.ex.InsertException;
import com.nw.fool.service.ex.OrderNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**菜单模块业务层的实现类*/
@Service  //@Service 注解：将当前类的对象交给spring来管理，自动创建对象以及对象的维护
public class OrderServiceImpl implements IOrderService {
    @Autowired(required = false)
    private OrderMapper orderMapper;
    @Override
    public void insert(Order order) {
        String cname=order.getCname();
        Order result=orderMapper.findByCname(cname);
        if(result != null){
            //抛出异常
            throw new CnameDuplicateException("菜名被占用");
        }
        //补全数据
        order.setCreatedUser(order.getCreatedUser());
        order.setModifiedUser(order.getModifiedUser());
        Date date = new Date();
        order.setCreatedTime(date);
        order.setModifiedTime(date);
        Integer rows = orderMapper.insert(order);
        if (rows !=1){
            throw new InsertException("插入今日菜单数据产生未知异常");
        }



    }

    @Override
    public Order findByCname(String cname) {
        Order order = orderMapper.findByCname(cname);
        if(order==null){
            throw new OrderNotFoundException("菜名不存在");
        }
        return order;
    }

    @Override
    public Integer update(Order order) {
        return orderMapper.update(order);
    }

    @Override
    public void deleteByCid(Integer cid) {
        Order result=orderMapper.findByCid(cid);
        if(result == null){
            throw new OrderNotFoundException("今日菜谱数据不存在");
        }
        Integer rows = orderMapper.deleteByCid(cid);
        if (rows !=1){
            throw new DeleteException("删除今日菜谱数据产生未知的异常");
        }
    }

    @Override
    public List<Order> findByTdtype(String tdtype){
        List<Order> order = orderMapper.findByTdtype(tdtype);
        if(order == null){
            throw new OrderNotFoundException("今日菜谱不存在");
        }
        return order;
    }

    @Override
    public Order findByCid(Integer cid) {
        Order order =  orderMapper.findByCid(cid);
        if(order == null){
            throw new OrderNotFoundException("您所要查找的菜谱不存在");
        }
        return order;
    }

    @Override
    public void updateSureByCids(List<Order> order){
        orderMapper.updateSureByCids(order);
//        return orderDto;

    }

    @Override
    public void insertOrderInfoList(List<Order> order) {
        orderMapper.insertOrderInfoList(order);
    }

    @Override
    public void preInsertOrderInfoList(List<Order> order) {
        orderMapper.preInsertOrderInfoList(order);
    }

    @Override
    public List<Order> findOrderByTdtypeAndTime(String tdtype) {
        List<Order> order =orderMapper.findOrderByTdtypeAndTime(tdtype);
        return order;
    }
}
