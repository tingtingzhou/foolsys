package com.nw.fool.dto;


import lombok.Data;

@Data
public class UpdatePassworDto {
    private Integer uid;
    private String oldPassword;
    private String newPassword;
}
