package com.nw.fool.controller;

import com.nw.fool.dto.UpdatePassworDto;
import com.nw.fool.entity.User;
import com.nw.fool.service.IUserService;
import com.nw.fool.util.JsonResult;
import com.nw.fool.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("users")
public class UserController extends BaseController{
    @Autowired
    private IUserService userService;
    @RequestMapping("reg")
    public JsonResult<Void> reg(@RequestBody User user){
        userService.reg(user);

        return new JsonResult<>(OK);
    }
//    @RequestMapping("login")
//    public JsonResult<User> login(@RequestParam String username,
//                                  @RequestParam String password,
//                                  HttpSession session){
    @RequestMapping("login")
    public JsonResult<User> login(@RequestBody User user,
                              HttpSession session){
         //调用业务对象的方法执行登录，并获取返回值


        User data = userService.login(user.getUsername(),user.getPassword());
//        TokenUtil.getToken()



        System.out.println("data=" +data);
        //登录成功后，将uid和username存入到HttpSession中
        session.setAttribute("uid",data.getUid());
        session.setAttribute("username",data.getUsername());
        session.setAttribute("status",data.getStatus());
        session.setAttribute("depart",data.getDepart());
        session.setAttribute("telephone",data.getTelephone());
        session.setAttribute("token",data.getToken());
        //获取session中绑定的数据
         System.out.println("Session中的uid=" + getUidFromSession(session));
         System.out.println("Session中的username=" + getUsernameFromSession(session));
         System.out.println("Session中的status=" + getStatusFromSession(session));
         System.out.println("Session中的depart=" + getDepartFromSession(session));
         System.out.println("Session中的telephone" + getTelephoneFromSession(session));
        System.out.println("Session中的token" + getTokenFromSession(session));

//        System.out.println("Password=" + password);


        return new JsonResult<>(OK,data);




    }
    @GetMapping("changePassword")
    public JsonResult<User> changePassword(String oldPassword,
                                           String newPassword,
                                           HttpSession session){
       Integer uid = getUidFromSession(session);
       System.out.println("改密码 "+uid);
       String username = getUsernameFromSession(session);
       userService.changePassword(uid,
                                username,
                                oldPassword,
                                newPassword);
       return new JsonResult<>(OK);


    }
    @RequestMapping("delete")
    public JsonResult<User> delete(@RequestBody User user){
        userService.delete(user.getUid());
        return new JsonResult<>(OK);
    }

    @GetMapping("/")
    public JsonResult<List<User>> findUserAll(User user){
       List<User> data= userService.findUserAll(user);
       return new JsonResult<>(OK,data);

    }

    @RequestMapping("updateUserInfoByUid")
    public JsonResult<User> updateUserInfoByUid(@RequestBody User user){
//        user.setModifiedUser(user.getModifiedUser());
//        user.setUid(user.getUid());
        userService.updateUserInfoByUid(user);
        return new JsonResult<>(OK);
    }


    //分页查询
    @GetMapping("/page")
    public Map<String,Object> findPage(@RequestParam Integer pageNum,
                                       @RequestParam Integer pageSize,
                                       @RequestParam String username){
        pageNum =(pageNum-1)*pageSize;
        List<User> data = userService.selectPage(pageNum,pageSize,username);
        Integer total =userService.selectUserTotal(username);
        Map<String,Object> res = new HashMap<>();
        res.put("data",data);
        res.put("total",total);
        System.out.println(res);
        return res;
    }

    @RequestMapping("loginsys")
    public JsonResult<Void> loginsys(String username, String password){
        userService.loginsys(username,password);
        return new JsonResult<>(OK);

    }

    @RequestMapping("findByTerm")
    public JsonResult<List<User>> findByTerm(@RequestBody User user){
        List<User> data=userService.findByTerm(user);
        return new JsonResult<>(OK,data);
    }

    @RequestMapping("updateAdminInfoByUid")
    public JsonResult<User> updateAdminInfoByUid(@RequestBody User user){
        userService.updateAdminInfoByUid(user);
        return new JsonResult<>(OK);
    }

    @PostMapping("updatePasswordInfoByUid")
    public JsonResult<User> updatePasswordInfoByUid(@RequestBody UpdatePassworDto updatePassworDto){
//        log.info("接收到的数据UpdatePassworDto:{}",updatePassworDto);
        userService.updatePasswordInfoByUid(updatePassworDto);
        return new JsonResult<>(OK);

    }

    @RequestMapping("insertUserList")
    public JsonResult<User> insertUserList(@RequestBody List<User> user){
        userService.insertUserList(user);
        return new JsonResult<>(OK);
    }


    @RequestMapping("loginByTelephone")
    public JsonResult<User> loginByTelephone (@RequestBody User user,
                                              HttpSession session){
        //调用业务对象的方法执行登录，并获取返回值

        User data = userService.loginByTelephone(user.getTelephone(),user.getPassword());
//        User data = userService.login(username,password);
        System.out.println("data=" +data);
        //登录成功后，将uid和username存入到HttpSession中
        session.setAttribute("uid",data.getUid());
        session.setAttribute("username",data.getUsername());
        session.setAttribute("status",data.getStatus());
        session.setAttribute("depart",data.getDepart());
        session.setAttribute("telephone",data.getTelephone());
        session.setAttribute("token",data.getToken());
        System.out.println();
        //获取session中绑定的数据
        System.out.println("Session中的uid=" + getUidFromSession(session));
        System.out.println("Session中的username=" + getUsernameFromSession(session));
        System.out.println("Session中的status=" + getStatusFromSession(session));
        System.out.println("Session中的depart=" + getDepartFromSession(session));
        System.out.println("Session中的telephone" + getTelephoneFromSession(session));

//        System.out.println("Password=" + password);

        return new JsonResult<>(OK,data);


    }

    @RequestMapping("findByUid")
    public JsonResult<User> findByUid(@RequestBody User user){
        userService.findByUid(user.getUid());
        return new JsonResult<>(OK);
    }

//    @RequestMapping("findUserInfoByUid")
//    public JsonResult<User> findUserInfoByUid(String uid){
//        userService.findUserInfoByUid(uid);
//        return new JsonResult<>(OK);
//    }




    /*
    @RequestMapping("reg")
    public JsonResult<Void> reg(User user){
        //创建响应结果对象
        JsonResult<Void> result = new JsonResult();
        try {
            userService.reg(user);
            result.setState(200);
            result.setMessage("用户注册成功");
        } catch (UsernameDuplicateException e) {
            result.setState(4000);
            result.setMessage("用户名被占用");
        } catch (InsertException e) {
            result.setState(5000);
            result.setMessage("注册时产生未知的异常");
        }
        return result;
    }*/
}
