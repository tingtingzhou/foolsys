package com.nw.fool.controller;

import com.nw.fool.service.ex.*;
import com.nw.fool.util.JsonResult;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpSession;

/**控制层类的基类*/
public class BaseController {
    /**操作成功的状态码*/
    public static final int OK = 200;
    //请求处理方法，这个方法的返回值就是需要传递给前端的数据
    //自动将异常对象传递给此方法的参数列表
    //当项目中产生了异常，被统一拦截到此方法中，这个方法此时就充当的是请求处理方法，方法返回值直接给到前端
    @ExceptionHandler(ServiceException.class)
    public JsonResult<Void> handleException(Throwable e){
        JsonResult<Void> result = new JsonResult<>(e);
        if(e instanceof UsernameDuplicateException){
            result.setState(4000);
            result.setMessage("用户名已经被占用");
        }else if(e instanceof InsertException){
            result.setState(5000);
            result.setMessage("注册时产生未知异常");
        }else if(e instanceof UserNotFoundException){
            result.setState(5001);
            result.setMessage("用户数据不存在异常");
        }else if(e instanceof PasswordNotMatchException){
            result.setState(5002);
            result.setMessage("用户名的密码错误的异常");
        }else if(e instanceof UpdateException){
            result.setState(5003);
            result.setMessage("更新数据产生未知异常");
        }else if(e instanceof DeleteException){
            result.setState(5004);
            result.setMessage("删除数据产生未知异常");
        }else if(e instanceof CnameDuplicateException){
            result.setState(5005);
            result.setMessage("菜名被占用异常");
        }else if(e instanceof OrderNotFoundException){
            result.setState(5006);
            result.setMessage("菜谱数据不存在的异常");
        }
        else if(e instanceof OrderInfoNotFoundException){
            result.setState(5007);
            result.setMessage("订餐数据数据不存在的异常");
        }else if(e instanceof OrderInfoHasExistedException){
            result.setState(5008);
            result.setMessage("订餐信息已存在的异常");
        }
        return result;
    }

    /**
     *获取用户登录的uid，从HttpSession对象中获取uid
     * @param session session HttpSession对象
     * @return 当前登录的用户的id
     */
    protected final Integer getUidFromSession(HttpSession session){
        return Integer.valueOf(session.getAttribute("uid").toString());
    }

    /**
     *获取用户登录的username，从HttpSession对象中获取用户名
     * @param session session HttpSession对象
     * @return 当前登录的用户名
     */
    protected final String getUsernameFromSession(HttpSession session){
        return session.getAttribute("username").toString();
    }

    /**
     * 获取用户登录的status，从HttpSession对象中获取状态码
     * @param session
     * @return
     */
    protected final String getStatusFromSession(HttpSession session){
        return session.getAttribute("status").toString();
    }

    /**
     * 获取用户登录的depart，从HttpSession对象中获取部门
     * @param session
     * @return
     */
    protected final String getDepartFromSession(HttpSession session){
        return session.getAttribute("depart").toString();
    }


    protected final String getTelephoneFromSession(HttpSession session){
        return session.getAttribute("telephone").toString();
    }

    protected final String getTokenFromSession(HttpSession session){
        return session.getAttribute("token").toString();

    }
}
