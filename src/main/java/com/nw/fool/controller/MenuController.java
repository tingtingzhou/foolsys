package com.nw.fool.controller;

import com.nw.fool.entity.Menu;
import com.nw.fool.entity.User;
import com.nw.fool.service.IMenuService;
import com.nw.fool.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("menus")
public class MenuController extends BaseController{
    @Autowired
    private IMenuService menuService;

    @RequestMapping("insert")
    public JsonResult<Void> insert(@RequestBody Menu menu){
        menuService.insert(menu);
        return new JsonResult<>(OK);
    }

    @GetMapping("findMenuAll")
    public JsonResult<List<Menu>> findMenuAll(Menu menu){
        List<Menu> data = menuService.findMenuAll(menu);
        return new JsonResult<>(OK,data);

    }

    @RequestMapping("updateMenuInfoByCid")
    public JsonResult<Menu> updateMenuInfoByCid(@RequestBody Menu menu){
        menuService.updateMenuInfoByCid(menu);
        return new JsonResult<>(OK);
    }

    @RequestMapping("deleteMenuByCid")
    public JsonResult<Menu> deleteMenuByCid(@RequestBody Menu menu){
        menuService.deleteMenuByCid(menu.getCid());
        return new JsonResult<>(OK);
    }

    @RequestMapping("findMenuByCname")
    public JsonResult<List<Menu>> findMenuByCname(@RequestBody Menu menu){
        List<Menu> data = menuService.findMenuByCname(menu);
        return new JsonResult<>(OK,data);

    }

    @RequestMapping("deleteMenuByCids")
    public JsonResult<Menu> deleteMenuByCids(@RequestBody List<Integer> cids){
        menuService.deleteMenuByCids(cids);
        return new JsonResult<>(OK);
    }

}
