package com.nw.fool.controller;

import com.nw.fool.entity.Order;
import com.nw.fool.entity.OrderInfo;
import com.nw.fool.entity.User;
import com.nw.fool.service.IOrderInfoService;
import com.nw.fool.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("orderinfo")
public class OrderInfoController extends BaseController{
    @Autowired
    private IOrderInfoService orderInfoService;
    @RequestMapping("insert")
    public JsonResult<Void> insert(@RequestBody OrderInfo orderInfo){

        orderInfoService.insert(orderInfo);
        return new JsonResult<>(OK);
    }

    @RequestMapping("findOrderInfoByDctype")
    public JsonResult<List<OrderInfo>> findOrderInfoByDctype(@RequestBody OrderInfo orderInfo){
        List<OrderInfo> data =orderInfoService.findOrderInfoByDctype(orderInfo.getDctype());
        return new JsonResult<>(OK,data);
    }

    @RequestMapping("deleteByDcidAndDctype")
    public JsonResult<OrderInfo> deleteByDcidAndDctype(@RequestBody OrderInfo orderInfo){
        orderInfoService.deleteByDcidAndDctype(orderInfo.getDcid(), orderInfo.getDctype());
        return new JsonResult<>(OK);

    }

    @GetMapping("findOrderInfoAll")
    public JsonResult<List<OrderInfo>> findOrderInfoAll(OrderInfo orderInfo){
        List<OrderInfo> data = orderInfoService.findOrderInfoAll(orderInfo);
        return new JsonResult<>(OK,data);

    }

    @RequestMapping("findOrderInfoByDctime")
    public JsonResult<List<OrderInfo>> findOrderInfoByDctime(String dctimeStart,String dctimeEnd){
        List<OrderInfo> data =orderInfoService.findOrderInfoByDctime(dctimeStart,dctimeEnd);
        return new JsonResult<>(OK,data);

    }

    @RequestMapping("findOrderInfoByUsername")
    public JsonResult<List<OrderInfo>> findOrderInfoByUsername(String username){
        List<OrderInfo> data = orderInfoService.findOrderInfoByUsername(username);
        return new JsonResult<>(OK,data);
    }

    @RequestMapping("findOrderInfoByConditions")
    public JsonResult<List<OrderInfo>> findOrderInfoByConditions(String dctimeYear, String dctimeMonth, String dcdepart, String username){
        List<OrderInfo> data = orderInfoService.findOrderInfoByConditions(dctimeYear,dctimeMonth,dcdepart,username);
        return new JsonResult<>(OK,data);
    }

    @RequestMapping("findOrderInfoBydctimeYearAnddctimeMonth")
    public JsonResult<List<OrderInfo>> findOrderInfoBydctimeYearAnddctimeMonth(String dctimeYear, String dctimeMonth){
        List<OrderInfo> data = orderInfoService.findOrderInfoBydctimeYearAnddctimeMonth(dctimeYear,dctimeMonth);
        return new JsonResult<>(OK,data);
    }

    @RequestMapping("findOrderInfoBydctimeYearAnddctimeMonthAndUid")
    public JsonResult<List<OrderInfo>> findOrderInfoBydctimeYearAnddctimeMonthAndUid(String dctimeYear, String dctimeMonth, Integer uid){
        List<OrderInfo> data = orderInfoService.findOrderInfoBydctimeYearAnddctimeMonthAndUid(dctimeYear,dctimeMonth,uid);
        return new JsonResult<>(OK,data);
    }


    @RequestMapping("findOrderInfoByUidAndDctime")
    public JsonResult<List<OrderInfo>> findOrderInfoByUidAndDctime(Integer uid,String dctype){
        List<OrderInfo> data = orderInfoService.findOrderInfoByUidAndDctime(uid,dctype);
        return new JsonResult<>(OK,data);
    }
    @RequestMapping("orderInfoCount")
    public JsonResult<List<OrderInfo>> orderInfoCount(String dctype){
        List<OrderInfo> data = orderInfoService.orderInfoCount(dctype);
        return new JsonResult<>(OK,data);

    }
}
