package com.nw.fool.controller;



import com.nw.fool.entity.Order;
import com.nw.fool.service.IOrderService;
import com.nw.fool.util.JsonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("orders")
public class OrderController extends BaseController{
    @Autowired
    private IOrderService orderService;
    @RequestMapping("insert")
    public JsonResult<Void> insert(Order order){
        orderService.insert(order);
        return new JsonResult<>(OK);
    }

    @RequestMapping("findByCname")
    public JsonResult<Order> findByCname(String cname){
        Order data=orderService.findByCname(cname);
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("deleteByCid")
    public JsonResult<Order> deleteByCid(Integer cid){
        orderService.deleteByCid(cid);
        return new JsonResult<>(OK);
    }

    /** 根据订餐类型查找今日菜谱 **/
    @RequestMapping("findByTdtype")
    public JsonResult<List<Order>> findByTdtype(@RequestBody Order order){
        List<Order> data = orderService.findByTdtype(order.getTdtype());
        return new JsonResult<>(OK, data);
    }

    @RequestMapping("findByCid")
    public JsonResult<Order> findByCid(@RequestBody Order order){
        Order data = orderService.findByCid(order.getCid());
        return new JsonResult<>(OK, data);
    }


    /** 根据cid和时间修改ccounted **/
    @RequestMapping("updateSureByCids")
    public JsonResult<Order> updateSureByCids(@RequestBody List<Order> order){

//        System.out.println(order.getCid());
//        System.out.println(order.getTdtype());
        orderService.updateSureByCids(order);

        return new JsonResult<>(OK);
    }

    @RequestMapping("insertOrderInfoList")
    public JsonResult<Order> insertOrderInfoList(@RequestBody List<Order> order){
        orderService.insertOrderInfoList(order);
        return new JsonResult<>(OK);
    }

    @RequestMapping("preInsertOrderInfoList")
    public JsonResult<Order> preInsertOrderInfoList(@RequestBody List<Order> order){
        orderService.preInsertOrderInfoList(order);
        return new JsonResult<>(OK);
    }

    @RequestMapping("findOrderByTdtypeAndTime")
    public JsonResult<List<Order>> findOrderByTdtypeAndTime(@RequestBody Order order){
        List<Order> data = orderService.findOrderByTdtypeAndTime(order.getTdtype());
        return new JsonResult<>(OK, data);
    }


}
