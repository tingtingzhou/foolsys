package com.nw.fool;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration //表示配置类
@SpringBootApplication
//@MapperScan注解指定当前项目中的Mapper接口路径的位置，在项目启动的时候加载所有接口
@MapperScan("com.nw.fool.mapper")
//@ComponentScan("com.nw.fool.mapper")
public class FoolApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoolApplication.class, args);
    }

}
