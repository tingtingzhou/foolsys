package com.nw.fool.entity;

import lombok.Data;

import java.io.Serializable;


/**
 * 菜谱表的实体类
 */

@Data
public class Menu extends BaseEntity implements Serializable {
    private Integer cid;
    private String cname;
    private String cprice;

}
