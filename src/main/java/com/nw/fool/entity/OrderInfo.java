package com.nw.fool.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**订餐信息实体类*/
@Data
public class OrderInfo extends BaseEntity implements Serializable {
    private Integer dcid;
    private Integer uid;
    private String username;
    private String dcstorey;
    private String dcdepart;
    private String dclist;
    private Double dcprice;
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date dctime;
    private Integer dccount;
    private String dctype;
    private String dctel;

}
