package com.nw.fool.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**今日菜谱实体类*/
@Data
public class Order extends BaseEntity implements Serializable {
    private Integer cid;
    private String cname;
    private Double cprice;
    private Integer ccount;
    private Integer ccounted;
    private String tdtype;
    private Date time;

}
