package com.nw.fool.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
/** 用户数据的实体类 */
@Data
public class User extends BaseEntity implements Serializable {
    private Integer uid;
    private String username;
    private String password;
    private String salt;
    private String telephone;
    private String depart;
    private Integer status;
    private Integer isDelete;
    private String token;

}
