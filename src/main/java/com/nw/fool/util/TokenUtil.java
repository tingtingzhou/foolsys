package com.nw.fool.util;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TokenUtil {


    /**
     * 过期时间为一个小时
     */
    private static final long EXPIRE_TIME =  1 * 60 * 1000;

    /**
     * token私钥  这里的密码随便写就可以
     */
    private static final String TOKEN_SECRET = "adm2345dfin234sdsdff";


    /**
     * 获取请求的token
     */


    public static String getToken(String username,String password) {
        String token = null;
        Date expiration = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        Map<String,Object> header = new HashMap<>();
        header.put("typ","JWT");
        header.put("alg","HS256");
        token = JWT.create()
                .withIssuer("auth0")
                .withHeader(header)
                //这个地方是你传的参数
                .withClaim("username",username)
                .withClaim("password",password)
                .withExpiresAt(expiration)
                .sign(Algorithm.HMAC256(TOKEN_SECRET));
        return token;

    }
    public static boolean verify(String token) {
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(TOKEN_SECRET)).withIssuer("auth0").build();
            DecodedJWT jwt = verifier.verify(token);
            System.out.println("认证通过：");
            System.out.println("过期时间：" + jwt.getExpiresAt());
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Token认证失败，需要重新登录！");
        }
        return false;
    }


}
