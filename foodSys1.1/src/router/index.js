import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/Login.vue'
import adminHome from '../components/adminHome.vue'
import userHome from '../components/userHome.vue'
import Users from '../components/Users.vue'
import Personal from '../components/Personal.vue'
import Notice from '../components/Notice.vue'
import EditMenu from '../components/Menu/EditMenu.vue'
import IssueMenu from '../components/Menu/IssueMenu.vue'
import OrderToday from '../components/Order/OrderToday.vue'
import OrderThree from '../components/Order/OrderThree.vue'
import Welcome from '../components/User/Welcome.vue'
import Individual from '../components/User/Individual.vue'
import OrderFood from '../components/User/OrderFood.vue'
import SearchOrder from '../components/User/SearchOrder.vue'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      redirect: 'login'
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/adminhome',
      component: adminHome,
      redirect: 'notice',
      children: [
        { path: '/users', component: Users },
        { path: '/personal', component: Personal },
        { path: '/notice', component: Notice },
        { path: '/editMenu', component: EditMenu },
        { path: '/issueMenu', component: IssueMenu },
        { path: '/orderToday', component: OrderToday },
        { path: '/orderThree', component: OrderThree }
      ]
    },
    {
      path: '/userhome',
      component: userHome,
      redirect: 'welcome',
      children: [
        { path: '/welcome', component: Welcome },
        { path: '/individual', component: Individual },
        { path: '/orderfood', component: OrderFood },
        { path: '/searchorder', component: SearchOrder }
      ]
    }
  ]
})

// 如果用户没有登录,则看不到主页,强制跳转到登录页面
// beforeEach参数:to-将要访问的路径//from-代表从哪个路径跳转而来//next是一个函数,代表放行(即是否允许)
// next()->放行, next('/login')->强制跳转
// router.beforeEach((to, from, next) => {
//   if (to.path === '/login') return next()
//   // 根据token判断用户是否已经登录
//   const tokenStr = window.sessionStorage.getItem('token')
//   if (!tokenStr) return next('/login')
//   next()
// })

export default router
