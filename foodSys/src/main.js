// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 导入全局样式表
import './assets/css/global.css'
import axios from 'axios'
import qs from 'qs'

// 配置请求的根路径
axios.defaults.baseURL = 'http://localhost:9090/'

// 为请求头添加一个字段保存用户登录token
axios.interceptors.request.use(config => {
  console.log("token得config",config)
  config.headers['token'] = sessionStorage.getItem("token")
  return config
}
)
axios.interceptors.response.use(config => {
  // console.log(config)
  if (config.data.code==50000){
    ElementUI.Message({
      type: 'error',
      message: 'token令牌已过期，请重新登录！'
    })
    router.push('./Login')
  }
  return config
})


Vue.prototype.$http = axios
// axios.defaults.withCredentials = true
Vue.use(ElementUI)
Vue.use(qs)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  render: h => h(App)
  // components: { App },
  // template: '<App/>'
})
